<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class LiveClock extends AbstractController
{
    /**
      * @Route("/clock")
      */
    public function clock()
    {
        date_default_timezone_set("Europe/Bucharest");
        $date = date("d/m/Y");
        $hour = date('H:i');


        return $this->render('clock.html.twig', [
            'date' => $date,
            'hour' => $hour
        ]);
    }

    /**
     * @Route("/ajax/time")
     */
    public function ajaxTime(){
        date_default_timezone_set("Europe/Bucharest");
        $hour = date('H:i:s');
        return new Response($hour);
    }
}