<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class SumNumbers
{

    /**
      * @Route("/sum")
      */
    public function suma()
    {
        $number1 = random_int(0, 100);
        $number2 = random_int(0, 100);
        $sum = $number1 + $number2;
        return new Response(
            '<html><body>'.$number1.' + '.$number2.' = '.$sum.'</body></html>'
        );
    }
}